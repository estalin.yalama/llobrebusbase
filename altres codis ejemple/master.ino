/*
 * 
 * Aquesta secció defineix el funcionament de les comunicacion en el rol de master.
 * Es a dir des del moment que decidim contactar a un escalu fins que en processem la resposta. 
 * 
 */

 int sendRequest(int adress, int op_code, byte &send_data, int data_length);
 int waitResponse(int adress, int op_code, byte &recive_data, int &data_length);

 int preccesOP1Response(byte *data);
 // implementar tots els processos de cada OP
